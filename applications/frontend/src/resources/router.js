'use strict';

import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import Blog from '../templates/pages/Blog/Blog.vue';
import Page from '../templates/pages/Page/Page.vue';

let router = new VueRouter({
	mode: 'hash',
	routes: [
		{
			path: '/',
			name: 'index'
		},
		{
			path: '/blog',
			name: "blog",
			component: Blog
		},
		{
			path: '/:page/:anchor?',
			name: 'page',
			component: Page
		}
	],
});

export default router;