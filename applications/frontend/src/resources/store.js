'use strict';

import Vue from 'vue';

import Vuex from 'vuex';

Vue.use(Vuex);

let store = new Vuex.Store({
	state: {
		xhrs: {},
		meta: null,
		pageTitle: null,
		previousPageTitle: null,
		previousPageTitleReturn: null,
		previousRoute: null,
		activeSidebar: false,
		activeSidebarTab: 'search',
		searchTitles: {
			info: 'Информация',
			post: 'Блог',
			project: 'Проекты',
			position: 'Ассортимент',
			teammate: 'Команда'
		}
	}
});

store.watch((state) => {
	return state.pageTitle
}, (newValue, oldValue) => {
	store.state.previousPageTitle = oldValue;
	document.querySelector('head title').innerHTML = newValue;
});


export default store;