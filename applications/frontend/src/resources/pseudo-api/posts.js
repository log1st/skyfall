'use strict';

let posts = [];

[
	{
		title: 'Представляем новое меню авторских кальянов Skyfall',
		description: 'Впервые с момента открытия, команда Skyfall решилась переработать кальянное меню: упростили выбор классических кальянов и представили новую коллекцию авторских',
		group: 'Skyfall Group',
		date: 1526688000,
		thumbnail: 'https://image.ibb.co/kZy7d8/skyfall_group.jpg'
	},
	{
		title: 'Как мы разрабатывали новую систему сочетания чая и кальяна',
		description: 'За неделю команда Skyfall продегустировала более 50 сортов чая, чтобы понять какие из них удачно сочетаются с каждой из категорий вкуса кальяна.',
		group: 'Skyfall Group',
		date: 1526688000,
		thumbnail: 'https://image.ibb.co/kZy7d8/skyfall_group.jpg'
	},
	{
		title: 'Как связаны Адель, агент 007 и кальяны?',
		description: 'Курил ли у нас Бонд и что за слова в нашем туалете — наконец отвечаем почему мы выбрали именно это название для нашего первого заведения.',
		group: 'Skyfall Group',
		date: 1526688000,
		thumbnail: 'https://image.ibb.co/kZy7d8/skyfall_group.jpg'
	},
	{
		title: 'Happy birthday, Skyfall — анонс события по случаю двух лет с открытия',
		group: 'Skyfall Group',
		date: 1526688000,
		primary: true,
		background: 'https://image.ibb.co/kZy7d8/skyfall_group.jpg'
	},
	{
		title: 'Секрет идеального салата от нашего шеф-повара',
		description: 'Гастрономический фаворит Clouds, Армэн Висчхулидзе, рассказал нам как сотворить правильный салат и обычных продуктов.',
		group: 'Интервью',
		date: 1526688000,
		thumbnail: 'https://image.ibb.co/kZy7d8/skyfall_group.jpg'
	},
	{
		title: 'Интерью с Борисом Краснощекастым',
		description: 'За чашкой Хунь Шунь Цзыня пообщались с основателем самого большого производства чая на cеверо-западе страны',
		group: 'Интервью',
		date: 1526688000,
		thumbnail: 'https://image.ibb.co/kZy7d8/skyfall_group.jpg'
	},
	{
		title: 'Мы попробовали зелёный чай с Уделки… и выжили!',
		group: 'Эксклюзив',
		date: 1526688000,
		primary: true,
		background: 'https://image.ibb.co/kZy7d8/skyfall_group.jpg'
	},
	{
		title: 'Негрони цвета заката: готовимся к лету с классическим коктейлем',
		description: 'В предверии жаркого времени года мы решили освежить вас историей одного из самых легендарных напитков XX века.',
		group: 'Истории',
		date: 1526688000,
		thumbnail: 'https://image.ibb.co/kZy7d8/skyfall_group.jpg'
	},
	{
		title: 'Интервью с Екатериной Змеялиной — главным декоратором в городе',
		description: 'Наш постоянный гость Катя поделилась опытом из индустрии, рассказала о главных трендах в индустрии',
		group: 'Интерью',
		date: 1526688000,
		thumbnail: 'https://image.ibb.co/kZy7d8/skyfall_group.jpg'
	},
	{
		title: 'Брось телефон: крик души о привычках в XXI веке',
		description: 'Прекращай играть в какие-то фермы, удали все чаты. Жизнь идёт, а ты снова залипаешь…',
		group: 'Истории',
		date: 1526688000,
		thumbnail: 'https://image.ibb.co/kZy7d8/skyfall_group.jpg'
	}
].map((post, i) => {
	post.link = {
		name: 'post',
		params: {
			alias: 'some-alias-' + (i + 1)
		}
	};
	
	posts.push(post);
});

export default {
	status: 200,
	response: posts
};