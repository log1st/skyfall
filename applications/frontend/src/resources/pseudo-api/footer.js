'use strict';

export default {
	status: 200,
	response: {
		phone: '+7 800 555 35 35',
		phoneDescription: 'Наш общий номер для получения справочной информации, бронирования мест и связи с менеджером проектов.',
		email: 'info@skyfallgroup.ru',
		emailDescription: 'Проверяем почту каждый день, отвечаем в течение 24 часов.',
		socials: [
			{
				key: 'vkontakte',
				link: 'https://vk.com/log1st'
			},
			{
				key: 'facebook',
				link: 'https://fb.com/sergeyperenets'
			},
			{
				key: 'telegram',
				link: 'https://telegram.me/@hehblya'
			},
			{
				key: 'instagram',
				link: 'https://instagram.com/log1st.ps'
			},
		],
		copyright: '© ООО «Скайфолл Групп», 2018',
		policyLink: 'https://vk.com/log1st',
		designer: 'Павел Сойфер',
		designerLink: 'https://vk.com/log1st',
		
		map: require('./pages').default.response.custom.map(item => {
			try {
				let page = require('./pages/' + item.route.params.page ).default.response,
					components = page.components.filter(item => {
						return item.type === 'delimiter';
					}).map(item => {
						return {
							label: item.meta.titleForFooter || item.meta.title,
							key: item.meta.cKey
						}
					});
				
				return {
					title: page.titleForFooter || page.title,
					route: item.route,
					components: components
				}
			}   catch(e) {
				console.log(e);
				return false;
			}
		}).filter(item => item !== false)
	}
}