export default {
	status: 200,
	response: [
		{
			title: 'Skyfall Group',
			description: 'Подробнее о нашей группе проектов, партнёрах и программе лояльности',
			image: 'https://image.ibb.co/kZy7d8/skyfall_group.jpg',
			iconLight: 'https://vk.com/doc383964643_466942758?hash=27cc8e3f55765a06b1&dl=f855f9e6bf3c22f0b3',
			iconDark: 'https://vk.com/doc383964643_466942759?hash=06be9b3c8373ae542a&dl=372d89a1fa95b0ede2',
			link: {
				name: 'page',
				params: {
					name: 'skyfall-group'
				}
			}
		},
		{
			title: 'High Tea',
			description: 'Чайные коллекции Skyfall и Clouds теперь доступны в продаже',
			image: 'https://image.ibb.co/cFWhBT/tea.jpg',
			link: {
				name: 'page',
				params: {
					name: 'high-tea'
				}
			}
		},
		{
			title: 'Кальян-бар Skyfall',
			description: 'Первый интеллигентный кальян-бар Петербурга',
			address: 'Лиговский проспект, 39/1',
			image: 'https://image.ibb.co/g6DJko/skyfall.jpg',
			iconLight: 'https://vk.com/doc383964643_466942755?hash=0c572b6d50ef440869&dl=2b0f56bb08065079c1',
			iconDark: 'https://vk.com/doc383964643_466942756?hash=65ff7986763204a7b1&dl=2c89472c4114bdcb4c',
			link: {
				name: 'page',
				params: {
					name: 'skyfall'
				}
			}
		},
		{
			title: 'Гастро-бар Clouds',
			description: 'Авторские коктейли, деликатесная кухня, уникальный интерьер',
			address: 'Лиговский проспект, 39/1',
			iconLight: 'https://vk.com/doc383964643_466942648?hash=61520596b578a6024e&dl=8fb80f4671debcd4db',
			iconDark: 'https://vk.com/doc383964643_466942754?hash=151669f7ffca4d5d9c&dl=c7845838364227a945',
			image: 'https://image.ibb.co/bPpr5o/clouds.jpg',
			link: {
				name: 'page',
				params: {
					name: 'clouds'
				}
			}
		},
		{
			title: 'Наш блог',
			description: 'Обозреваем индустрию, берём интерью у всяких людей',
			image: 'https://image.ibb.co/mQufy8/blog.jpg',
			link: {
				name: 'blog',
			}
		},
	]
}