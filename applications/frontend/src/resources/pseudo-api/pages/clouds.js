'use strict';


let menu = [
	{
		iconDark: 'https://vk.com/doc383964643_469091850?hash=b43bb9f66e3f7c004a&dl=680ff8567f523d7d8e',
		iconLight: 'https://vk.com/doc383964643_469096214?hash=c3b8eb0a41da84095b&dl=384f66ee2617af775c',
		background: 'https://image.ibb.co/cFWhBT/tea.jpg',
		title: 'Чайная карта',
		groups: Array(4).fill({
			title: 'Шу Пуэр',
			positions: Array(10).fill({
				title: 'Лапсанг Сушонг',
				price: 400
			})
		})
	},
	{
		iconDark: 'https://vk.com/doc383964643_469091850?hash=b43bb9f66e3f7c004a&dl=680ff8567f523d7d8e',
		iconLight: 'https://vk.com/doc383964643_469096214?hash=c3b8eb0a41da84095b&dl=384f66ee2617af775c',
		background: 'https://image.ibb.co/cFWhBT/tea.jpg',
		title: 'Десерты',
		positions: Array(6).fill({
			title: 'Чизкейки',
			description: 'Чизкейки разные крутые классные всякие разные черные белые и, конечно, красные, Нью Йорк, клубничный',
			price: 190
		})
	},
	{
		iconDark: 'https://vk.com/doc383964643_469091850?hash=b43bb9f66e3f7c004a&dl=680ff8567f523d7d8e',
		iconLight: 'https://vk.com/doc383964643_469096214?hash=c3b8eb0a41da84095b&dl=384f66ee2617af775c',
		background: 'https://image.ibb.co/cFWhBT/tea.jpg',
		title: 'Холодные напитки',
		positions: Array(4).fill({
			title: 'Домашний лимонад',
			description: 'Огуречный, огуречно-базиликовый, маракуйя, ананас с кокосом, малиновый, клубничный, клубнично-имбирный, лимонный, лаймовый, арбузный, тархун, дюшес, красный апельсин, грейпфрутовый, личи с корицей, яблочный пирог, лемонграсс',
			price: 490,
			tags: ['200мл', 'Вегетарианское']
		})
	}
];

export default {
	status: 200,
	response: {
		title: 'Clouds',
		titleReturn: 'к Clouds',
		components: [
			{
				type: 'delimiter',
				meta: {
					cKey: 'concept',
					title: 'Концепция заведения',
					titleForFooter: 'Концепция',
					icon: 'https://vk.com/doc383964643_467321624?hash=98b45c7f761d0c9dd4&dl=fd9b9e5209796ccae0'
				}
			},
			{
				type: 'article',
				meta: {
					title: 'Кухня высокого уровня, коктейли на любую погоду',
					mainPart: 'Clouds — гастрономическое явление в погоде Петербурга. Волна вкусовых впечатлений и шторм авторских коктейлей перевнут ваше представление о походе в бар.',
					extendedPart: [
						'На волне нашего первого заведения — кальян-бара Skyfall — мы продолжили историю сочетаний, на этот раз взявшись за полный ресторанный сегмент и тематику авторских коктейлей. В Clouds ваши рецепторы получат прилив впечатлений. Тщательно подобранное меню деликатесов отлично заходит к четвётому бокалу Олд Фэшнда.',
						'В любую погоду, в любое время года или дня, в Clouds ты найдёшь богатый выбор блюд и напитков, которые гарантированно снижают осадки банальности и вызывают торнато новых открытий. Ты будешь приятно удивлён — это мы обещаем.'
					],
					crossPosts: [
						{
							id: 1,
							image: 'https://image.ibb.co/cFWhBT/tea.jpg',
							body: 'Секрет идеального салата от нашего шеф-повара',
						},
						{
							id: 2,
							image: 'https://image.ibb.co/cFWhBT/tea.jpg',
							body: 'Негрони цвета заката: готовимся к лету с классическим коктейлем',
						}
					]
				}
			},
			{
				type: 'slider',
				meta: {
					slides: [
						{
							image: 'https://image.ibb.co/g6DJko/skyfall.jpg',
						},
						{
							image: 'https://image.ibb.co/mQufy8/blog.jpg'
						},
						{
							image: 'https://image.ibb.co/cFWhBT/tea.jpg'
						},
						{
							image: 'https://image.ibb.co/kZy7d8/skyfall_group.jpg'
						}
					]
				}
			},
			{
				type: 'delimiter',
				meta: {
					cKey: 'menu',
					title: 'Меню Clouds',
					titleForFooter: 'Меню',
					subTitle: menu.length + ' категорий',
					icon: 'https://vk.com/doc383964643_468980284?hash=6392771f91e300801f&dl=c0139fd6d9f7e2babb',
					lightBackground: true,
					gray: true
				}
			},
			{
				type: 'menu',
				meta: {
					categories: menu,
					file: {
						link: 'https://vk.com/log1st',
						size: 2.5 * 1024,
						icon: 'https://vk.com/doc383964643_469092218?hash=e87b883201d308b708&dl=9d9030b4369caa3a36'
					}
				}
			},
			{
				type: 'delimiter',
				meta: {
					cKey: 'team',
					title: 'Команда',
					icon: 'https://vk.com/doc383964643_468288708?hash=05eff1920eed1b8538&dl=e900ea77bbe72aec5a'
				}
			},
			{
				type: 'team',
				meta: {
					title: 'В детстве летали во сне, а теперь — в Clouds',
					description: 'Мы собрали команду истинных гурманов и ценителей, способных постичь глубину нашей концепции и желающих поделиться ей с миром. Официант не просто подскажет, а расскажет, а бармен выслушает и приготовит что-то особенное в вашу честь.',
					members: [
						{
							name: 'Елена Кантемировская',
							position: 'Управляющий',
							photo: 'https://image.ibb.co/cFWhBT/tea.jpg'
						},
						{
							name: 'Александра Тырса',
							position: 'Официант',
							photo: 'https://image.ibb.co/cFWhBT/tea.jpg'
						},
						{
							name: 'Армэн Висчхулидзе',
							position: 'Шеф-повар',
							photo: 'https://image.ibb.co/cFWhBT/tea.jpg'
						},
						{
							name: 'Анастасия Никонова',
							position: 'Официант',
							photo: 'https://image.ibb.co/cFWhBT/tea.jpg'
						},
						{
							name: 'Евгений Поднебесных',
							position: 'Бренд-менеджер',
							photo: 'https://image.ibb.co/cFWhBT/tea.jpg'
						},
						{
							name: 'Екатерина Вилайчук',
							position: 'Шеф-бармен',
							photo: 'https://image.ibb.co/cFWhBT/tea.jpg'
						},
						{
							name: 'Василий Иск',
							position: 'Официант',
							photo: 'https://image.ibb.co/cFWhBT/tea.jpg'
						},
						{
							name: 'Константин Долгомешающий',
							position: 'Бармен',
							photo: 'https://image.ibb.co/cFWhBT/tea.jpg'
						},
						{
							name: 'Захар Культурин',
							position: 'Повар',
							photo: 'https://image.ibb.co/cFWhBT/tea.jpg'
						},
					]
				}
			},
			{
				type: 'map',
				meta: {
					markerX: 59.932570398604156,
					markerY: 30.361307859420776,
					centerX: 59.9316,
					centerY: 30.3630,
					address: 'Лиговский проспект, 39/1',
					city: 'Санкт-Петербург',
					phone: '+7 921 933-00-40',
					icon: 'https://vk.com/doc383964643_469098073?hash=273efa1aea3fa9120e&dl=a009fd40d3e898e817',
					workPeriods: [
						{
							days: 'Воскресенье – четверг:',
							time: 'с 13:00 до 01:00'
						},
						{
							days: 'Пятница, суббота:',
							time: 'с 13:00 до 05:00'
						},
					],
					socials: [
						{
							key: 'vkontakte',
							link: 'https://vk.com/log1st'
						},
						{
							key: 'instagram',
							link: 'https://instagram.com/log1st.ps'
						}
					]
				}
			}
		]
	}
}