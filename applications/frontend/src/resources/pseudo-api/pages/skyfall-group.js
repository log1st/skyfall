'use strict';

export default {
	status: 200,
	response: {
		title: 'Skyfall Group',
		titleReturn: 'к Skyfall Group',
		components: [
			{
				type: 'delimiter',
				meta: {
					cKey: 'history',
					title: 'Наша история',
					icon: 'https://vk.com/doc383964643_467321624?hash=98b45c7f761d0c9dd4&dl=fd9b9e5209796ccae0'
				}
			},
			{
				type: 'article',
				meta: {
					title: 'Мы просто хотели прокачать индустрию и культуру отдыха',
					mainPart: 'Однажды, в пятницу вечером, два товарища бродили по центру Петербурга в поисках спокойного места, где можно было отдохнуть, попробовать что-то интересно, найти приятного собеседника и на секунду забыть о проблемах бытия. Место они не нашли, посему решили открыть своё.',
					extendedPart: [
						'Лежа на панцирнотвердой спине, он видел, стоило ему приподнять голову, свой коричневый, выпуклый, разделенный дугообразными чешуйками живот, на верхушке которого еле держалось.'
					],
					staff: [
						{
							photo: 'https://image.ibb.co/cFWhBT/tea.jpg',
							name: 'Константин Длиннофамильный',
							position: 'Со-основатель'
						},
						{
							photo: 'https://image.ibb.co/cFWhBT/tea.jpg',
							name: 'Михаил Желиковенский',
							position: 'Со-основатель'
						},
						{
							photo: 'https://image.ibb.co/cFWhBT/tea.jpg',
							name: 'Артур Кледонин',
							position: 'Менеджер проектов'
						}
					],
					crossPosts: [
						{
							id: 1,
							image: 'https://image.ibb.co/cFWhBT/tea.jpg',
							body: 'Интервью с Екатериной Змеялиной — главным декоратором в городе',
						},
						{
							id: 2,
							image: 'https://image.ibb.co/cFWhBT/tea.jpg',
							body: 'Брось телефон: крик души о привычках в XXI веке',
						}
					]
				}
			},
			{
				type: 'poster',
				meta: {
					image: 'http://dl4.joxi.net/drive/2018/06/12/0001/1243/128219/19/66aa33f808.png'
				}
			},
			{
				type: 'delimiter',
				meta: {
					cKey: 'partners',
					title: 'Партнёры',
					gray: true,
					lightBackground: true,
					icon: 'https://vk.com/doc383964643_467332217?hash=3a1baf29c8b2ee22e9&dl=ba339fb922c8c82774'
				}
			},
			{
				type: 'partners',
				meta: {
					title: 'Компании, с которыми мы прошли вместе огонь, воду и какие-то трубы',
					description: 'Мы всегда открыты для предложений сотрудничества и кооперации. В одиночку что-либо изменить невероятно сложно, но вместе сможем развивать индустрию, выходить на новый уровень.',
					partners: [
						{
							logo: 'https://i.imgur.com/bLrsRAX.png',
							description: 'Гордо носим Рокетноски и предлагаем специальные бонусы всем держателям рыжей карты.'
						},
						{
							logo: 'https://i.imgur.com/EREuMRN.png',
							description: 'Держим расчётный счёт в банке с мировой историей, также предлагаем кэшбек для клиентов Райффайзена.'
						},
						{
							logo: 'https://i.imgur.com/luiuc4B.png',
							description: 'Наша система лояльности основана на решении от Plazius, а пользователи их приложения получают бонусы.'
						},
						{
							logo: 'https://i.imgur.com/zjlvyQM.png',
							description: 'Являемся официальным партнёров, представлены в разделах «Рестораны» и «Развлечения» уже более двух лет.'
						},
						{
							logo: 'https://i.imgur.com/cB02sR8.png',
							description: 'Полтора года работаем вместе с Softsmoke, чтобы предоставить нашим клиентам максимальное качество кальянов.'
						},
						{
							logo: 'https://i.imgur.com/QmQxOm6.png',
							description: 'Участвуем в самом большом гастрономическом событии Петербурга на постоянной основе.'
						},
					
					]
				}
			},
			{
				type: 'delimiter',
				meta: {
					cKey: 'loyalty',
					title: 'Карта лояльности',
					icon: 'https://vk.com/doc383964643_468937024?hash=cd04e3db314f1ea028&dl=3f67b545cc8e57fbf3'
				}
			},
			{
				type: 'loyalty',
				meta: {
					title: 'Получите свою карту Skyfall Group в одном из наших заведений',
					description: 'Постоянным гостям мы предлагаем карту лояльности, на которую мы зачисляем бонусные баллы. Посещайте все заведения Skyfall Group, получайте уникальные предложения, расплачивайтесь баллами при следующем посещении.',
					background: 'https://pp.userapi.com/c850016/v850016393/98d3/5_1V6BFkfBM.jpg',
					advantages: [
						{
							icon: 'https://vk.com/doc383964643_468943359?hash=2f79f2249139514ffc&dl=89090b9592d0bb716a',
							title: 'Возвращаем до 10% бонусными баллами на карту после посещения'
						},
						{
							icon: 'https://vk.com/doc383964643_468943367?hash=aec2d3c7dc8549ac89&dl=2bef102fee146ad67d',
							title: 'Карта действует во всех заведениях Skyfall Group'
						},
						{
							icon: 'https://vk.com/doc383964643_468943361?hash=06cf547a4a24d3746e&dl=00860e755e918716a0',
							title: 'Прогрессивная система бонусов, больше посещений — больше вернём на карту'
						}
					],
					file: {
						title: 'Условия программы',
						extension: 'PDF',
						size: 2 * 1024,
						link: 'http://vk.com/hehblya'
					}
				}
			}
		]
	}
}