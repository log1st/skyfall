'use strict';

let menu = [
	{
		iconDark: 'https://vk.com/doc383964643_469091850?hash=b43bb9f66e3f7c004a&dl=680ff8567f523d7d8e',
		iconLight: 'https://vk.com/doc383964643_469096214?hash=c3b8eb0a41da84095b&dl=384f66ee2617af775c',
		background: 'https://image.ibb.co/cFWhBT/tea.jpg',
		title: 'Кальянная карта',
		positions: Array(5).fill({
			title: 'Фирменный кальян Skyfall',
			description: 'Классический кальян на воде с любым сочетанием табаков из нашей коллекции в глиняной чаше.',
			price: 1200
		})
	},
	{
		iconDark: 'https://vk.com/doc383964643_469091850?hash=b43bb9f66e3f7c004a&dl=680ff8567f523d7d8e',
		iconLight: 'https://vk.com/doc383964643_469096214?hash=c3b8eb0a41da84095b&dl=384f66ee2617af775c',
		background: 'https://image.ibb.co/cFWhBT/tea.jpg',
		title: 'Чайная карта',
		groups: Array(5).fill({
			title: 'Шу Пуэр',
			positions: Array(10).fill({
				title: 'Лапсанг Сушонг',
				price: 400
			})
		})
	},
	{
		iconDark: 'https://vk.com/doc383964643_469091850?hash=b43bb9f66e3f7c004a&dl=680ff8567f523d7d8e',
		iconLight: 'https://vk.com/doc383964643_469096214?hash=c3b8eb0a41da84095b&dl=384f66ee2617af775c',
		background: 'https://image.ibb.co/cFWhBT/tea.jpg',
		title: 'Десерты',
		positions: Array(3).fill({
			title: 'Чизкейки',
			description: 'Чизкейки разные крутые классные всякие разные черные белые и, конечно, красные, Нью Йорк, клубничный',
			price: 190
		})
	},
	{
		iconDark: 'https://vk.com/doc383964643_469091850?hash=b43bb9f66e3f7c004a&dl=680ff8567f523d7d8e',
		iconLight: 'https://vk.com/doc383964643_469096214?hash=c3b8eb0a41da84095b&dl=384f66ee2617af775c',
		background: 'https://image.ibb.co/cFWhBT/tea.jpg',
		title: 'Холодные напитки',
		positions: Array(3).fill({
			title: 'Домашний лимонад',
			description: 'Огуречный, огуречно-базиликовый, маракуйя, ананас с кокосом, малиновый, клубничный, клубнично-имбирный, лимонный, лаймовый, арбузный, тархун, дюшес, красный апельсин, грейпфрутовый, личи с корицей, яблочный пирог, лемонграсс',
			price: 490,
			tags: ['200мл', 'Вегетарианское']
		})
	}
];

export default {
	status: 200,
	response: {
		title: 'Skyfall',
		titleReturn: 'к Skyfall',
		components: [
			{
				type: 'delimiter',
				meta: {
					cKey: 'concept',
					title: 'Концепция заведения',
					titleForFooter: 'Концепция',
					icon: 'https://vk.com/doc383964643_467321624?hash=98b45c7f761d0c9dd4&dl=fd9b9e5209796ccae0'
				}
			},
			{
				type: 'article',
				meta: {
					title: 'Путешествие в мир аромата и вкусовых сочетаний',
					mainPart: 'В нашем первом заведении мы постарались реализовать действительно интеллигентную атмосферу, где ты почувствуешь себя комфортно.',
					extendedPart: [
						'Это место для дискуссий, обсуждений, споров и дебатов, но также будет удобным для спокойного отдыха, зависания, чила или сна. Наши мастера всегда помогут выбрать наилучшее сочетание аромата и чая, расскажут о культуре курения кальяна, историю сортов чая.',
						'Мы переплели массовую и элитарную кальянную культуру, дополнив яркими вкусами чая и вознесли процесс курения в ранг искусства. Бар Skyfall — заведение для стильного отдыха, приятного общения, деловых встреч и pre-party. '
					],
					crossPosts: [
						{
							id: 1,
							image: 'https://image.ibb.co/cFWhBT/tea.jpg',
							body: 'Представляем новое меню авторских кальянов Skyfall',
						},
						{
							id: 2,
							image: 'https://image.ibb.co/cFWhBT/tea.jpg',
							body: 'Как мы разрабатывали новую систему сочетания чая и кальяна',
						}
					]
				}
			},
			{
				type: 'slider',
				meta: {
					slides: [
						{
							image: 'https://image.ibb.co/g6DJko/skyfall.jpg',
							panorama: 'https://yandex.ru/map-widget/v1/-/CBuYMFgn3A'
						},
						{
							image: 'https://image.ibb.co/mQufy8/blog.jpg'
						},
						{
							image: 'https://image.ibb.co/cFWhBT/tea.jpg'
						},
						{
							image: 'https://image.ibb.co/kZy7d8/skyfall_group.jpg'
						}
					]
				}
			},
			{
				type: 'delimiter',
				meta: {
					cKey: 'menu',
					title: 'Меню Skyfall',
					titleForFooter: 'Меню',
					subTitle: menu.length + ' категорий',
					icon: 'https://vk.com/doc383964643_468980284?hash=6392771f91e300801f&dl=c0139fd6d9f7e2babb',
					lightBackground: true,
					gray: true
				}
			},
			{
				type: 'menu',
				meta: {
					categories: menu,
					file: {
						link: 'https://vk.com/log1st',
						size: 2.5 * 1024,
						icon: 'https://vk.com/doc383964643_469092218?hash=e87b883201d308b708&dl=9d9030b4369caa3a36'
					}
				}
			},
			{
				type: 'delimiter',
				meta: {
					cKey: 'team',
					title: 'Команда',
					icon: 'https://vk.com/doc383964643_468288708?hash=05eff1920eed1b8538&dl=e900ea77bbe72aec5a'
				}
			},
			{
				type: 'team',
				meta: {
					title: 'Вот эти ребята знают толк в кальянах и чае',
					description: 'Как полагается первому интеллигентному, наши мастера и гуру не только собаку скурили на кальянах и чае, но и всегда поддержат беседой, расскажут историю чая и подберут для вас самое интересное сочетание вкусов.',
					members: [
						{
							name: 'Ангелина Светлаковская',
							position: 'Чайный гуру',
							photo: 'https://image.ibb.co/cFWhBT/tea.jpg'
						},
						{
							name: 'Алексей Ефимов',
							position: 'Кальян-мастер',
							photo: 'https://image.ibb.co/cFWhBT/tea.jpg'
						},
						{
							name: 'Женя Склифосовский',
							position: 'Кальян-мастер',
							photo: 'https://image.ibb.co/cFWhBT/tea.jpg'
						},
						{
							name: 'Константин Сергеев',
							position: 'Бренд-шеф, кальянный мастер',
							photo: 'https://image.ibb.co/cFWhBT/tea.jpg'
						},
						{
							name: 'Станислав Бальшмаковский',
							position: 'Чайный бог',
							photo: 'https://image.ibb.co/cFWhBT/tea.jpg'
						},
					]
				}
			},
			{
				type: 'outsourcing',
				meta: {
					background: 'https://image.ibb.co/cFWhBT/tea.jpg',
					delimiter: 'Аутсорсинг от Skyfall',
					icon: 'https://vk.com/doc383964643_469020553?hash=54ac2532583f6db4fe&dl=c08fed8dbf253fce98',
					title: 'Вкусовое безумие теперь и на вашей площадке',
					description: 'Гости вашего заведения или мероприятия смогут насладиться сочетанием авторских кальянов и редких чаёв от мастеров Skyfall.',
					buttonText: 'Узнать больше',
					routeParam: 'outsourcing'
				}
			},
			{
				type: 'map',
				meta: {
					markerX: 59.932570398604156,
					markerY: 30.361307859420776,
					centerX: 59.9316,
					centerY: 30.3630,
					address: 'Лиговский проспект, 39/1',
					city: 'Санкт-Петербург',
					phone: '+7 981 939-00-39',
					icon: 'https://vk.com/doc383964643_469043399?hash=672e99aa2c91326ace&dl=0c21f58c72a50c2dcd',
					workPeriods: [
						{
							days: 'Воскресенье – четверг:',
							time: 'с 14:00 до 00:00'
						},
						{
							days: 'Пятница, суббота:',
							time: 'с 14:00 до 04:00'
						},
					],
					socials: [
						{
							key: 'vkontakte',
							link: 'https://vk.com/log1st'
						},
						{
							key: 'instagram',
							link: 'https://instagram.com/log1st.ps'
						}
					]
				}
			}
		]
	}
}