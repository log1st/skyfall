'use strict';

export default {
	status: 200,
	response: {
		custom: [
			{
				title: 'Skyfall Group',
				route: {
					name: 'page',
					params: {
						page: 'skyfall-group'
					}
				}
			},
			{
				title: 'Skyfall',
				route: {
					name: 'page',
					params: {
						page: 'skyfall'
					}
				}
			},
			{
				title: 'Аутсорсинг',
				route: {
					name: 'page',
					params: {
						page: 'outsourcing'
					}
				}
			},
			{
				title: 'Clouds',
				route: {
					name: 'page',
					params: {
						page: 'clouds'
					}
				}
			},
		]
	}
}