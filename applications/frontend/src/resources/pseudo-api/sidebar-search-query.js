let projects = require('./sidebar-projects').default,
	posts = require('./posts').default;

export default {
	status: 200,
	response: [
		{
			type: 'project',
			meta: projects.response[0]
		},
		{
			type: 'position',
			meta: {
				title: 'Ассортимент High Tea',
				icon: 'https://pp.userapi.com/c824500/v824500307/127bc5/aS3Af1-sZTo.jpg',
				description: 'Смотреть чай на странице проекта',
				link: {
					name: 'position',
					params: {
						name: 'high-tea'
					}
				}
			}
		},
		{
			type: 'position',
			meta: {
				title: 'Меню кальян-бара Skyfall',
				icon: 'https://pp.userapi.com/c824500/v824500307/127bc5/aS3Af1-sZTo.jpg',
				description: 'Смотреть чай на странице проекта',
				link: {
					name: 'position',
					params: {
						name: 'skyfall'
					}
				}
			}
		},
		{
			type: 'position',
			meta: {
				title: 'Меню гастро-бара Clouds',
				icon: 'https://pp.userapi.com/c824500/v824500307/127bc5/aS3Af1-sZTo.jpg',
				description: 'Смотреть чай на странице проекта',
				link: {
					name: 'position',
					params: {
						name: 'clouds'
					}
				}
			}
		},
		{
			type: 'teammate',
			meta: {
				name: 'Ангелина Светлаковская',
				photo: 'https://pp.userapi.com/c824500/v824500307/127bc5/aS3Af1-sZTo.jpg',
				description: 'Чайный гуру из кальян-бара Skyfall',
				link: {
					name: 'teammate',
					params: {
						name: 'angelina-svetlakovskaya'
					}
				}
			}
		},
		{
			type: 'teammate',
			meta: {
				name: 'Константин Лийст',
				photo: 'https://pp.userapi.com/c824500/v824500307/127bc5/aS3Af1-sZTo.jpg',
				description: 'Божество чайное из гастро-бара Clouds',
				link: {
					name: 'teammate',
					params: {
						name: 'konstantin-liyst'
					}
				}
			}
		},
		{
			type: 'info',
			meta: {
				iconDarkBg: 'https://image.ibb.co/cnOsQo/skyfall_mark_copy.png',
				iconLightBg: 'https://image.ibb.co/eYOKqo/skyfall_mark_copy.png',
				title: 'Кальян-бар Skyfall',
				address: 'Лиговский проспект, 39/1',
				phone: '+7 (981) 939-00-39',
				workTime: 'Воскресенье – четверг: с 14:00 до 0:00\nПятница, суббота: с 14:00 до 04:00',
				link: {
					name: 'info',
					params: {
						name: 'skyfall'
					}
				}
			}
		},
		
		{
			type: 'post',
			meta: posts.response[0]
		},
		{
			type: 'post',
			meta: posts.response[1]
		},
		{
			type: 'post',
			meta: posts.response[2]
		}
	]
}