'use strict';

import Vue from 'vue';

Vue.component('logo', require('../../images/svg/logo.svg'));

[
	'chat', 'cross', 'grid', 'loupe', 'loader', 'delete', 'chevron', 'arrow', 'arrow-revert', 'bookmark', 'carte', 'user', 'map-pin', 'plus', 'chevron-rounded',
	'vkontakte', 'facebook', 'telegram', 'instagram', 'chevron-rounded-o', 'play'
].map(icon => {
	Vue.component('icon-' + icon, require('../../images/svg/' + icon + '.svg'))
});