'use strict';

['Black', 'BlackItalic', 'Bold', 'BoldItalic', 'Regular', 'Italic'].map(item => {
	require('fonts/PlayFair/PlayfairDisplay-' + item + '.ttf');
});

['UltraBold', 'Bold', 'SemiBold', 'Regular', 'Light'].map(item => {
	require('fonts/GillSans/GillSans-' + item + '.ttf');
});

['Medium'].map(item => {
	require('fonts/GillSansNova/Gill Sans Nova ' + item + '.ttf');
});
