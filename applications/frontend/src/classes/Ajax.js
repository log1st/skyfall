'use strict';

import store from '../resources/store';

class Ajax {
	static defaultOptions() {
		return {
			url: '/',
			type: 'get',
			dataType: 'html',
			data: {},
			contentType: 'application/x-www-form-urlencoded',
			xhr: null
		}
	};
	
	constructor(options) {
		options = Object.assign({}, Ajax.defaultOptions(), options);
		
		return new Promise((resolve, reject) => {
			
			let xhr = null;
			
			if (options.xhr !== null) {
				if (options.xhr.isPrototypeOf(XMLHttpRequest)) {
					xhr = options.xhr;
					xhr.abort();
				} else if (store.state.xhrs.hasOwnProperty(options.xhr)) {
					xhr = store.state.xhrs[options.xhr];
					xhr.abort();
				} else {
					xhr = new XMLHttpRequest();
					store.state.xhrs[options.xhr] = xhr;
				}
			}
			if (xhr === null) {
				xhr = new XMLHttpRequest();
			}
			
			if (options.type === 'get' && Object.keys(options.data).length) {
				let query = '',
					esc = encodeURIComponent;
				
				Object.keys(options.data).map(key => {
					return esc(key) + '=' + esc(options.data[key]);
				}).join('&');
				
				options.url = options.url + '?' + query;
			}
			
			
			xhr.open(options.type, options.url, true);
			
			xhr.onload = () => {
				if (xhr.status === 200) {
					let response = xhr.response;
					
					if (options.dataType === 'json') {
						try {
							response = JSON.parse(response);
						} catch (e) {
							reject(xhr.response);
						}
					}
					
					resolve(response);
				} else {
					reject(xhr.responseText);
				}
			};
			
			xhr.setRequestHeader('XHR', '1');
			
			xhr.onerror = () => {
				reject(xhr.responseText);
			};
			
			if (options.type.toLowerCase() === 'post') {
				let data;
				
				let tokenParam = document.querySelector('meta[name="csrf-param"]').content,
					tokenValue = document.querySelector('meta[name="csrf-token"]').content;
				
				
				if (options.contentType === false) {
					options.contentType = 'application/x-www-form-urlencoded';
				}
				
				if (options.data instanceof FormData) {
					data = options.data;
					if (!data.has(tokenParam)) {
						data.append(tokenParam, tokenValue);
					}
					options.contentType = 'multipart/form-data';
				} else if (options.contentType === 'multipart/form-data') {
					data = new FormData();
					
					for (let key in options.data) {
						if (options.data.hasOwnProperty(key)) {
							data.append(key, options.data[key]);
						}
					}
					if (!data.has(tokenParam)) {
						data.append(tokenParam, tokenValue);
					}
				} else if (options.contentType === 'application/x-www-form-urlencoded') {
					data = options.data;
					
					if (!data.hasOwnProperty(tokenParam)) {
						data[tokenParam] = tokenValue;
					}
					
					data = $.param(data);
				} else if (options.contentType === 'application/json') {
					data = options.data;
					
					if (!data.hasOwnProperty(tokenParam)) {
						data[tokenParam] = tokenValue;
					}
				}
				
				if (options.contentType === 'multipart/form-data') {
					options.contentType = null;
				}
				
				if (options.contentType) {
					xhr.setRequestHeader('Content-Type', options.contentType);
				}
				
				
				xhr.send(data);
			} else {
				xhr.send();
			}
		})
	};
}

export default Ajax;