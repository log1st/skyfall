'use strict';

import './resources/assets/fonts';
import './resources/assets/svg-images';
import './styles/index.scss';

import Vue from 'vue';
import store from './resources/store';
import router from './resources/router';
import IndexTemplate from './templates/Container.vue';

import moment from 'moment';

moment.locale('ru');

let VueTouch = require('vue-touch');
Vue.use(VueTouch, {name: 'v-touch'});

let meta = require('./resources/pseudo-api/meta').default;
store.state.meta = meta;

let Application = new Vue({
	el: '#app',
	components: {
		IndexTemplate
	},
	template: '<IndexTemplate/>',
	store,
	router
});

document.addEventListener('DOMContentLoaded', () => {
	let attachFastClick = require('fastclick');
	attachFastClick.attach(document.body);
});

if (module.hot) {
	module.hot.accept();
}

export default Application;