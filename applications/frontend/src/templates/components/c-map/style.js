'use strict';

export default [
	{
		"featureType": "all",
		"elementType": "all",
		"stylers": [
			{
				"saturation": "-100"
			},
			{
				"lightness": "30"
			},
			{
				"invert_lightness": true
			}
		]
	},
	{
		"featureType": "landscape",
		"elementType": "geometry",
		"stylers": [
			{
				"lightness": "-48"
			},
			{
				"weight": "0.01"
			}
		]
	},
	{
		"featureType": "landscape",
		"elementType": "geometry.fill",
		"stylers": [
			{
				"lightness": "-86"
			},
			{
				"weight": "0.01"
			},
			{
				"gamma": "1.84"
			}
		]
	},
	{
		"featureType": "landscape",
		"elementType": "geometry.stroke",
		"stylers": [
			{
				"lightness": "25"
			},
			{
				"gamma": "0.8"
			},
			{
				"weight": "0.01"
			}
		]
	},
	{
		"featureType": "landscape",
		"elementType": "labels",
		"stylers": [
			{
				"gamma": "1.12"
			}
		]
	},
	{
		"featureType": "poi",
		"elementType": "geometry.stroke",
		"stylers": [
			{
				"weight": "7.36"
			}
		]
	},
	{
		"featureType": "poi",
		"elementType": "labels.text.fill",
		"stylers": [
			{
				"lightness": "-37"
			},
			{
				"weight": "0.01"
			}
		]
	},
	{
		"featureType": "poi",
		"elementType": "labels.text.stroke",
		"stylers": [
			{
				"gamma": "0.00"
			},
			{
				"lightness": "-100"
			},
			{
				"weight": "1.25"
			}
		]
	},
	{
		"featureType": "poi.attraction",
		"elementType": "all",
		"stylers": [
			{
				"visibility": "off"
			}
		]
	},
	{
		"featureType": "poi.business",
		"elementType": "all",
		"stylers": [
			{
				"visibility": "off"
			},
			{
				"weight": "1.33"
			}
		]
	},
	{
		"featureType": "poi.government",
		"elementType": "all",
		"stylers": [
			{
				"visibility": "off"
			}
		]
	},
	{
		"featureType": "poi.medical",
		"elementType": "all",
		"stylers": [
			{
				"visibility": "off"
			}
		]
	},
	{
		"featureType": "poi.place_of_worship",
		"elementType": "all",
		"stylers": [
			{
				"visibility": "off"
			}
		]
	},
	{
		"featureType": "poi.school",
		"elementType": "all",
		"stylers": [
			{
				"visibility": "off"
			}
		]
	},
	{
		"featureType": "poi.sports_complex",
		"elementType": "all",
		"stylers": [
			{
				"visibility": "off"
			}
		]
	},
	{
		"featureType": "road",
		"elementType": "geometry",
		"stylers": [
			{
				"lightness": "12"
			}
		]
	},
	{
		"featureType": "road",
		"elementType": "geometry.fill",
		"stylers": [
			{
				"visibility": "on"
			},
			{
				"lightness": "-8"
			}
		]
	},
	{
		"featureType": "road",
		"elementType": "labels",
		"stylers": [
			{
				"weight": "0.01"
			},
			{
				"lightness": "14"
			}
		]
	},
	{
		"featureType": "road",
		"elementType": "labels.text.fill",
		"stylers": [
			{
				"weight": "0.01"
			},
			{
				"lightness": "0"
			},
			{
				"gamma": "0.66"
			},
			{
				"visibility": "on"
			}
		]
	},
	{
		"featureType": "road",
		"elementType": "labels.text.stroke",
		"stylers": [
			{
				"weight": "1.25"
			},
			{
				"gamma": "0.00"
			}
		]
	},
	{
		"featureType": "road",
		"elementType": "labels.icon",
		"stylers": [
			{
				"lightness": "94"
			},
			{
				"saturation": "-98"
			},
			{
				"visibility": "off"
			}
		]
	},
	{
		"featureType": "road.local",
		"elementType": "all",
		"stylers": [
			{
				"visibility": "on"
			}
		]
	},
	{
		"featureType": "road.local",
		"elementType": "geometry.fill",
		"stylers": [
			{
				"lightness": "-39"
			}
		]
	},
	{
		"featureType": "transit.station",
		"elementType": "geometry",
		"stylers": [
			{
				"weight": "0.01"
			}
		]
	},
	{
		"featureType": "transit.station",
		"elementType": "labels",
		"stylers": [
			{
				"visibility": "on"
			},
			{
				"saturation": "-100"
			},
			{
				"lightness": "40"
			},
			{
				"weight": "0.91"
			}
		]
	},
	{
		"featureType": "transit.station",
		"elementType": "labels.text.stroke",
		"stylers": [
			{
				"weight": "0.01"
			},
			{
				"lightness": "-48"
			}
		]
	},
	{
		"featureType": "transit.station",
		"elementType": "labels.icon",
		"stylers": [
			{
				"lightness": "28"
			},
			{
				"weight": "0.01"
			},
			{
				"visibility": "on"
			},
			{
				"saturation": "-100"
			},
			{
				"gamma": "0.17"
			}
		]
	},
	{
		"featureType": "transit.station.bus",
		"elementType": "labels.icon",
		"stylers": [
			{
				"saturation": "-99"
			},
			{
				"lightness": "-43"
			},
			{
				"visibility": "off"
			}
		]
	},
	{
		"featureType": "transit.station.rail",
		"elementType": "labels.icon",
		"stylers": [
			{
				"visibility": "on"
			},
			{
				"lightness": "-15"
			},
			{
				"weight": "0.01"
			}
		]
	}
];