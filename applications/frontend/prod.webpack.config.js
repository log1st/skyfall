'use strict';

let
	config = {},
	path = require('path'),
	webpack = require('webpack'),
	srcDir = path.resolve(__dirname, 'src'),
	distDir = path.resolve(__dirname, 'dist'),
	
	HTMLWebpackPlugin = require('html-webpack-plugin'),

	CleanWebpackPlugin = require('clean-webpack-plugin'),
	
	ExtractTextPlugin = require("extract-text-webpack-plugin"),
	
	{VueLoaderPlugin} = require('vue-loader'),
	
	alias = {
		'vue$': 'vue/dist/vue.esm.js',
		'fonts': path.resolve(srcDir, 'fonts'),
		'css-utils': path.resolve(srcDir, 'styles/utilities/index.scss')
	};

config = Object.assign({}, config, {
	mode: 'production',
	entry: [
		'babel-polyfill',
		'./index.js'
	],
	context: srcDir,
	resolve: {
		extensions: ['.js', '.scss', '.vue'],
		alias
	},
	resolveLoader: {
		alias
	},
	output: {
		library: '[name]',
		path: distDir,
		filename: '[name].[hash].js',
	},
	module: {
		rules: [
			{
				test: /\.scss$/,
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: [
						{
							loader: "css-loader",
							options: {
								minimize: false,
								sourceMap: true
							}
						}, {
							loader: 'postcss-loader',
							options: {
								sourceMap: true
							}
						}, {
							loader: "sass-loader",
							options: {
								sourceMap: true
							}
						}
					]
				}),
			},
			{
				test: /^(?!.*\.generated\.ttf$).*\.ttf$/,
				use: ['style-loader', 'css-loader', 'fontface-loader'],
			},
			{
				test: /\.generated.(woff(2)?|ttf|eot|svg|otf)([?\w.#_=]+)?$/,
				exclude: path.resolve(srcDir, 'images/'),
				loader: 'file-loader',
				options: {
					publicPath: '/',
					name: '[name].[ext]'
				}
			},
			{
				test: /\.js$/,
				use: 'babel-loader'
			},
			{
				test: /\.pug$/,
				use: 'pug-plain-loader'
			},
			{
				test: /\.vue$/,
				use: [
					{
						loader: 'vue-loader',
						options: {
							loaders: {
								js: {
									loader: 'babel-loader',
								}
							}
						}
					},
				],
			},
			{
				test: /\.(svg)$/,
				loader: 'vue-svg-loader',
				include: path.resolve(srcDir, 'images/svg'),
				exclude: path.resolve(srcDir, 'fonts'),
				options: {
					svgo: {
						plugins: [
							{
								removeDoctype: true
							},
							{
								removeComments: true
							},
							{
								cleanupIDs: false
							},
						]
					}
				}
			},
			{
				test: /\.(jpg|jpeg|png|gif|ico)$/,
				loader: 'file-loader'
			}
		]
	},
	plugins: [
		new CleanWebpackPlugin(['dist/*.*']),
		new HTMLWebpackPlugin({
			title: 'log1st',
			filename: 'index.html',
			xhtml: true,
			template: path.resolve(srcDir, 'index.html')
		}),
		new webpack.HotModuleReplacementPlugin(),
		new VueLoaderPlugin,
		new ExtractTextPlugin({
			filename: 'bundle.css',
			allChunks: true
		}),
	],
});

module.exports = config;